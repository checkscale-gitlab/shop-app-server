from app import app, db
from passlib.apps import custom_app_context as pwd_context
from itsdangerous import (TimedJSONWebSignatureSerializer
    as Serializer, BadSignature, SignatureExpired)


class Product(db.Model):
    __tablename__ = 'product'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('users.id'))
    title = db.Column(db.String(64))
    price = db.Column(db.Float(10, 2))
    description = db.Column(db.String(128))
    image_url = db.Column(db.String(128))

    def __init__(self, user_id,title, price, description, image_url, is_favorite):
        self.user_id = user_id
        self.title = title
        self.price = price
        self.description = description
        self.image_url = image_url
        self.is_favorite = is_favorite

    def __repr__(self):
        return '<Product {}>'.format(self.title)


class Cartorder(db.Model):
    __tablename__ = 'cartorder'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('users.id'))
    amount = db.Column(db.Float(10, 2), nullable=False)
    datetime = db.Column(db.DateTime(), nullable=False)

    # cart_id = db.Column(db.Integer, db.ForeignKey('cart.id'))
    cart = db.relationship("Cart", lazy='joined')

    def __init__(self, user_id, amount, datetime):
        self.user_id = user_id
        self.amount = amount
        self.datetime = datetime

    def __repr__(self):
        return '<Order {}>'.format(self.datetime)


class Cart(db.Model):
    __tablename__ = 'cart'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(64), nullable=False)
    price = db.Column(db.Float(10, 2), nullable=False)
    quantity = db.Column(db.Integer(), nullable=False)
    cart_order_id = db.Column(db.Integer(), db.ForeignKey('cartorder.id'))

    cart_order = db.relationship('Cartorder', lazy='joined')

    def __init__(self, cart_order_id, title, price, quantity):
        self.cart_order_id = cart_order_id
        self.title = title
        self.price = price
        self.quantity = quantity

    def __repr__(self):
        return '<Cart {}>'.format(self.title)


class Users(db.Model):
    expiration = 600

    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.String(32), index = True)
    password_hash = db.Column(db.String(128))

    def get_user_id(self):
        return self.id

    def get_user_expiration(self):
        return self.expiration

    def hash_password(self, password):
        self.password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password_hash)

    def generate_auth_token(self, expiration = 600):
        s = Serializer(app.config['SECRET_KEY'], expires_in = expiration)
        return s.dumps({ 'id': self.id })

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None # valid token, but expired
        except BadSignature:
            return None # invalid token
        user = Users.query.get(data['id'])
        return user

    def __repr__(self):
        return '<Users {}>'.format(self.username)


class Favorite(db.Model):
    __tablename__ = 'favorite'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('users.id'))
    product_id = db.Column(db.Integer(), db.ForeignKey('product.id'))
    is_favorite = db.Column(db.Boolean())

    def __init__(self, user_id, product_id, is_favorite):
        self.user_id = user_id
        self.product_id = product_id
        self.is_favorite = is_favorite

    def __repr__(self):
        return '<Favorite {}>'.format(self.is_favorite)

