CREATE TABLE "users" (
  "id" SERIAL PRIMARY KEY,
  "username" varchar(32) UNIQUE,
  "password_hash" varchar(128)
);

CREATE TABLE "product" (
  "id" SERIAL PRIMARY KEY,
  "user_id" int,
  "title" varchar(50),
  "price" "numeric(10, 2)",
  "description" varchar(255),
  "image_url" varchar(255)
);

CREATE TABLE "cartorder" (
  "id" SERIAL PRIMARY KEY,
  "user_id" int,
  "amount" "numeric(10, 2)",
  "datetime" timestamp
);

CREATE TABLE "cart" (
  "id" SERIAL PRIMARY KEY,
  "cart_order_id" int,
  "title" varchar(50),
  "price" "numeric(10, 2)",
  "quantity" int
);

CREATE TABLE "favorite" (
  "id" SERIAL PRIMARY KEY,
  "user_id" int,
  "product_id" integer,
  "is_favorite" boolean
);

ALTER TABLE "product" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "cartorder" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "cart" ADD FOREIGN KEY ("cart_order_id") REFERENCES "cartorder" ("id");

ALTER TABLE "favorite" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "favorite" ADD FOREIGN KEY ("product_id") REFERENCES "product" ("id");

ALTER TABLE "cart" ADD FOREIGN KEY ("cart_order_id") REFERENCES "product" ("title");
