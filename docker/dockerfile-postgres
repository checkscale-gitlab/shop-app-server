FROM debian:buster

# default postgres port
ARG PPORT=5432
ARG PV=11

RUN apt-get update \
    && apt-get install -y apt-utils

RUN apt-get update \
    && apt-get install -y \
    ipython \
    net-tools \
    postgresql-${PV} \
    postgresql-contrib-${PV} \
    postgresql-plpython-${PV} \
    postgresql-server-dev-${PV} \
    python-crypto \
    python-openssl \
    vim-gtk3

# Run the rest of the commands as the ``postgres``
# user created by the ``postgres-${PV}``
# package when it was ``apt-get installed``
USER postgres

# Adjust PostgreSQL configuration so that remote connections to the
# database are possible.
RUN echo "local   all             all                                     trust " > /etc/postgresql/${PV}/main/pg_hba.conf
RUN echo "host    all             all             127.0.0.1/32            trust " >> /etc/postgresql/${PV}/main/pg_hba.conf
RUN echo "host    all             all             ::1/128                 trust " >> /etc/postgresql/${PV}/main/pg_hba.conf
RUN echo "host    all             all             all                     trust " >> /etc/postgresql/${PV}/main/pg_hba.conf
RUN echo "listen_addresses='*'" >> /etc/postgresql/${PV}/main/postgresql.conf
RUN echo "port=${PPORT}" >> /etc/postgresql/${PV}/main/postgresql.conf

# Expose the PostgreSQL port
EXPOSE ${PPORT}

COPY ./database/create_database.sql /tmp/create_database.sql

RUN pg_ctlcluster ${PV} main start && \
    cat /var/log/postgresql/postgresql-${PV}-main.log && \
    psql -f /tmp/create_database.sql && \
    pg_ctlcluster ${PV} main stop
